<?php


namespace Filler\Controller;


use Filler\Service\PageService;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Views\Twig;

class PageController
{
    /* @var Twig */
    private $twig;

    /* @var PageService */
    private $pageService;

    public function __construct(ContainerInterface $container)
    {
        $this->twig = $container->get('view');
        $this->pageService = $container->get('pageService');
    }

    public function page(Response $response, $slug = "index") {
        return $this->twig->render($response, 'page.html.twig', [
            'page' => $this->pageService->get($slug)
        ]);
    }
}