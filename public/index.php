<?php

use DI\Container;
use Filler\Controller\PageController;
use Filler\Repository\PageRepository;
use Filler\Service\PageService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

define('ROOT', __DIR__ . '/../');

require __DIR__ . '/../vendor/autoload.php';

// Create Container
$container = new Container();
AppFactory::setContainer($container);

// Set view in Container
$container->set('view', function() {
    return Twig::create('views', ['cache' => 'cache/twig']);
});

$container->set('pageRepository', new PageRepository());
$container->set('pageService', new PageService($container->get('pageRepository')));
$container->set('pageController', new PageController($container));

// Create App
$app = AppFactory::create();

// Add Twig-View Middleware
$app->add(TwigMiddleware::createFromContainer($app));

$app->get('/', function (Request $request, Response $response, $args) {
    return $this->get('pageController')->page($response);
});

$app->get('/{slug}', function (Request $request, Response $response, $args) {
    return $this->get('pageController')->page($response, $args['slug']);
});

$app->run();